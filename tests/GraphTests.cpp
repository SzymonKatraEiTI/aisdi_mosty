#include "Graph.hpp"

#include <vector>
#include <cstddef>
#include <utility>

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

BOOST_AUTO_TEST_SUITE(GraphTests)

BOOST_AUTO_TEST_CASE(WhenConstructed_NodesCount)
{
	Graph g(3);

	BOOST_CHECK(g.getNodesCount() == 3);
}

BOOST_AUTO_TEST_CASE(WhenConstructed_ThenEdgesCountEquals0)
{
	Graph g(3);

	BOOST_CHECK(g.getEdgesCount() == 0);
}

BOOST_AUTO_TEST_CASE(WhenEdgeInserted_ThenEdgesCountIncremented)
{
	Graph g(3);

	g.insertEdge(0, 2);

	BOOST_CHECK(g.getEdgesCount() == 1);
}

BOOST_AUTO_TEST_CASE(WhenSeveralEdgesInserted_ThenEdgesCountIsAppropriate)
{
	Graph g(4);

	g.insertEdge(1, 2);
	g.insertEdge(2, 0);
	g.insertEdge(0, 1);
	g.insertEdge(0, 3);

	BOOST_CHECK(g.getEdgesCount() == 4);
}

BOOST_AUTO_TEST_CASE(WhenEdgeOutOfRangeInserted1_ThenExceptionIsThrown)
{
	Graph g(3);

	BOOST_CHECK_THROW(g.insertEdge(3, 0), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(WhenEdgeOutOfRangeInserted2_ThenExceptionIsThrown)
{
	Graph g(3);

	BOOST_CHECK_THROW(g.insertEdge(0, 5), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(WhenGraphNotConnected1_ReturnsFalse)
{
	Graph g(3);

	BOOST_CHECK(g.isConnected() == false);
}
BOOST_AUTO_TEST_CASE(WhenGraphNotConnected2_ReturnsFalse)
{
	Graph g(4);

	g.insertEdge(0, 1);
	g.insertEdge(2, 3);

	BOOST_CHECK(g.isConnected() == false);
}

BOOST_AUTO_TEST_CASE(WhenGraphConnected_ReturnsTrue)
{
	Graph g(3);

	g.insertEdge(0, 1);
	g.insertEdge(1, 2);

	BOOST_CHECK(g.isConnected() == true);
}

BOOST_AUTO_TEST_CASE(CheckExpansiveBridges1)
{
	Graph g(3);

	g.insertEdge(0, 1);
	g.insertEdge(1, 2);
	g.insertEdge(2, 0);

	BOOST_CHECK(g.isExpansiveBridge(0, 1) == false);
	BOOST_CHECK(g.isExpansiveBridge(1, 2) == false);
	BOOST_CHECK(g.isExpansiveBridge(2, 0) == false);
}

BOOST_AUTO_TEST_CASE(CheckExpansiveBridges2)
{
	Graph g(4);

	g.insertEdge(0, 1);
	g.insertEdge(1, 2);
	g.insertEdge(2, 3);
	g.insertEdge(3, 0);
	g.insertEdge(0, 2);

	BOOST_CHECK(g.isExpansiveBridge(0, 1) == false);
	BOOST_CHECK(g.isExpansiveBridge(1, 2) == false);
	BOOST_CHECK(g.isExpansiveBridge(2, 3) == false);
	BOOST_CHECK(g.isExpansiveBridge(3, 0) == false);
	BOOST_CHECK(g.isExpansiveBridge(0, 2) == true);
	BOOST_CHECK(g.isExpansiveBridge(2, 0) == true);
}

BOOST_AUTO_TEST_CASE(CheckAllExpansiveBridges1)
{
	Graph g(4);

	g.insertEdge(0, 1);
	g.insertEdge(1, 2);
	g.insertEdge(2, 3);
	g.insertEdge(3, 0);
	g.insertEdge(0, 2);

	auto result = g.getExpansiveBridges();

	BOOST_CHECK(result.size() == 1);
	BOOST_CHECK((result[0].first == 0 && result[0].second == 2) || (result[0].first == 2 && result[0].second == 0));
}

BOOST_AUTO_TEST_SUITE_END()