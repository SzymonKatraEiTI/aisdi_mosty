#include "Graph.hpp"

#include <stdexcept>
#include <stack>
#include <limits>

Graph::Node::Node(std::size_t i)
	: index(i)
{
}

Graph::Graph(std::size_t nodesCount)
	: m_nodesCount(nodesCount)
{
	m_nodes.reserve(nodesCount);
	for (size_t i = 0; i < nodesCount; i++)
	{
		m_nodes.push_back(Node(i));
	}

	m_edges.reserve(nodesCount);
	for (size_t i = 0; i < nodesCount; i++)
	{
		m_edges.push_back(std::vector<Node*>());
	}
}

size_t Graph::getNodesCount() const
{
	return m_nodesCount;
}

size_t Graph::getEdgesCount() const
{
	return m_initialEdges.size();
}

void Graph::insertEdge(size_t a, size_t b)
{
	if (a >= m_nodesCount)
	{
		throw std::out_of_range("a is out of range!");
	}
	if (b >= m_nodesCount)
	{
		throw std::out_of_range("b is of of range!");
	}

	m_edges[a].push_back(&m_nodes[b]);
	m_edges[b].push_back(&m_nodes[a]);

	m_initialEdges.push_back(std::pair<std::size_t, std::size_t>(a, b));
}

bool Graph::isConnected()
{
	size_t sizeMax = std::numeric_limits<std::size_t>::max();

	return visitReachable(sizeMax, sizeMax) == m_nodesCount;
}

bool Graph::isExpansiveBridge(size_t a, size_t b)
{
	return !isConnected(a, b);
}

std::vector<std::pair<std::size_t, std::size_t>> Graph::getExpansiveBridges()
{
	std::vector<std::pair<std::size_t, std::size_t>> output;

	for (const auto& i : m_initialEdges)
	{
		if (isExpansiveBridge(i.first, i.second))
		{
			output.push_back(i);
		}
	}

	return output;
}


/////////////////////////////
//     PRIVATE METHODS     //
/////////////////////////////

bool Graph::isConnected(size_t ignoreA, size_t ignoreB)
{
	size_t visited = visitReachable(ignoreA, ignoreB);

	return visited == 0 || // when no nodes visited then graph is empty
		   visited + 2 == m_nodesCount; // + 2 because we ignore 2 nodes
}

size_t Graph::visitReachable(size_t ignoreA, size_t ignoreB)
{
	clearVisitedFlags();
	size_t visitedCount = 0;

	std::stack<Node*> remainingNodes;

	size_t firstIndex = 0;
	while (firstIndex == ignoreA || firstIndex == ignoreB) firstIndex++;
	if (firstIndex >= m_nodesCount) return 0;

	Node* current = &m_nodes[firstIndex];
	
	remainingNodes.push(current);
	current->isVisited = true;
	visitedCount++;

	while (!remainingNodes.empty())
	{
		current = remainingNodes.top();
		remainingNodes.pop();

		std::vector<Node*>& neighbors = m_edges[current->index];
		for (std::vector<Node*>::iterator i = neighbors.begin(); i != neighbors.end(); i++)
		{
			Node* next = *i;

			if (next->isVisited) continue;
			if (next->index == ignoreA || next->index == ignoreB) continue;

			remainingNodes.push(next);
			next->isVisited = true;
			visitedCount++;
		}
	}

	return visitedCount;
}

void Graph::clearVisitedFlags()
{
	for (Node& i : m_nodes)
	{
		i.isVisited = false;
	}
}
