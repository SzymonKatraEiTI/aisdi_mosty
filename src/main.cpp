#include <iostream>
#include "Interface.hpp"

int main()
{
	Interface interface(std::cin, std::cout);
	interface.run();
}