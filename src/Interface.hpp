#include <iostream>
#include <vector>
#include "Graph.hpp"

class Interface
{
private:
	std::istream& m_inputStream;
	std::ostream& m_outputStream;

public:
	Interface(std::istream& inputStream, std::ostream& outputStream);

	void run();

private:
	Graph readGraph();
	void writeResult(std::vector<std::pair<std::size_t, std::size_t>> result);
};