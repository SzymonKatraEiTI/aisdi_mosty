#include <vector>
#include <utility>
#include <cstddef>

class Graph
{
private:
	struct Node
	{
	public:
		size_t index;
		bool isVisited;

		Node(std::size_t i);
	};

	std::vector<Node> m_nodes;
	std::vector<std::vector<Node*>> m_edges;
	std::vector<std::pair<std::size_t, std::size_t>> m_initialEdges;
	size_t m_nodesCount;

public:
	Graph(std::size_t nodesCount);

	size_t getNodesCount() const;
	size_t getEdgesCount() const;

	void insertEdge(size_t a, size_t b);

	bool isConnected();
	bool isExpansiveBridge(size_t a, size_t b);
	std::vector<std::pair<std::size_t, std::size_t>> getExpansiveBridges();

private:
	size_t visitReachable(size_t ignoreA, size_t ignoreB);
	bool isConnected(size_t ignoreA, size_t ignoreB);
	void clearVisitedFlags();
};