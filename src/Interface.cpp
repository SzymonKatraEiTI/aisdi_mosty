#include "Interface.hpp"

#include <sstream>

Interface::Interface(std::istream& inputStream, std::ostream& outputStream)
	: m_inputStream(inputStream), m_outputStream(outputStream)
{
}

void Interface::run()
{
	Graph graph = readGraph();

	auto result = graph.getExpansiveBridges();

	writeResult(result);
}


/////////////////////////////
//     PRIVATE METHODS     //
/////////////////////////////

Graph Interface::readGraph()
{
	std::size_t count;
	m_inputStream >> count;
	m_inputStream.ignore();

	Graph graph(count);

	std::string input;
	while (std::getline(m_inputStream, input))
	{
		if (input == "") break;

		std::stringstream lineStream;
		lineStream << input;

		std::size_t a, b;
		lineStream >> a >> b;
		graph.insertEdge(a, b);
	}

	return graph;
}

void Interface::writeResult(std::vector<std::pair<std::size_t, std::size_t>> result)
{
	for (const auto& i : result)
	{
		m_outputStream << i.first << " " << i.second << std::endl;
	}
}
