#!/bin/bash

if [ $# -eq 0 ]
then
	printf "Podaj sciezke do pliku wykonywalny jako argument, np. ./check.sh /home/user/pliki/aisdiMosty\n"
	
	exit -1
fi

rm -r myout 2> /dev/null
mkdir myout

for number in {0..8}
do
	infile="in/in$number.txt"
	outfile="myout/out$number.txt"
	$1 < $infile > $outfile
	
	compareto="out/out$number.txt"
	
	echo "TEST NR $number"
	diff $outfile $compareto
	printf "\n"
done

printf "Pusta linia po tescie oznacza ze output sie zgadza\n"
printf "W przeciwnym wypadku wyswietlane sa roznice\n\n"

exit 0
